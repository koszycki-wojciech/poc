package com.wojtek.poc.multithread.calculation;

import java.util.concurrent.Callable;

/**
 *
 * @author Wojciech Koszycki
 */
public class Imigrant implements Callable<Double> {

    Hammer counter;
    int percent;
    int value;

    public Imigrant(Hammer counter, int percent, int value) {
        this.counter = counter;
        this.percent = percent;
        this.value = value;
    }

    @Override
    public Double call() throws Exception {
        return counter.crashRock(value, percent);
    }

}
